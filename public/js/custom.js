$("#newParticipantDetails").hide();
$("#returningParticipantDetails").hide();

$("#newParticipant").click(function () {
    $("#newParticipantDetails").show();
    $("#returningParticipantDetails").hide();
});

$("#returningParticipant").click(function () {
    $("#newParticipantDetails").hide();
    $("#returningParticipantDetails").show();
});

function submit(){
	if($('select[name=registrationDate]').val() == null || $('select[name=registrationDate]').val() == ''){
		swal('Warning', 'Please select a registration date', 'warning');
	}else if($('input[name=newParticipant]:checked').val() == null || $('input[name=newParticipant]:checked').val() == ''){
		swal('Warning', 'Please specify if you are new or returning', 'warning');
	}else if($('input[name=newParticipant]:checked').val() == 'New'){
		if($('textarea[name=referral]').val() == null || $('textarea[name=referral]').val() == ''){
			swal('Warning', 'Please tell us how you heard about the challenge', 'warning');
		}else{
			if($('input[name=firstName]').val() == null || $('input[name=firstName]').val() == ''){
				swal('Warning', 'Please enter your first name', 'warning');
			}else if($('input[name=lastName]').val() == null || $('input[name=lastName]').val() == ''){
				swal('Warning', 'Please enter your last name', 'warning');
			}else if($('input[name=gender]:checked').val() == null || $('input[name=gender]:checked').val() == ''){
				swal('Warning', 'Please enter your gender', 'warning');
			}else if($('input[name=age]').val() == null || $('input[name=age]').val() == ''){
				swal('Warning', 'Please enter your age', 'warning');
			}else if($('input[name=email]').val() == null || $('input[name=email]').val() == ''){
				swal('Warning', 'Please enter your email', 'warning');
			}else if($('input[name=paidByOther]:checked').val() == null || $('input[name=paidByOther]:checked').val() == ''){
				swal('Warning', 'Please specify if someone paid for you', 'warning');
			}else if($('input[name=paidByOther]:checked').val() == 1){
				if($('input[name=paidByOtherName]').val() == null || $('input[name=paidByOtherName]').val() == ''){
					swal('Warning', 'Please enter the name of the person that paid for you', 'warning');
				}else if($('input[name=facebookGroup]:checked').val() == null || $('input[name=facebookGroup]:checked').val() == ''){
					swal('Warning', 'Please say if you would like to join the Facebook group', 'warning');
				}else if($('input[name=sharePhone]:checked').val() == null || $('input[name=sharePhone]:checked').val() == ''){
					swal('Warning', 'Please say if you would like to share your phone number', 'warning');
				}else if($('input[name=sharePhone]:checked').val() == 1){
					if($('input[name=phone]').val() == null || $('input[name=phone]').val() == ''){
						swal('Warning', 'Please enter your phone number', 'warning');
					}else if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
						swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
					}else{
						$.ajax({
					        url: '/submit-questionnaire',
					        type: "post",
					        headers: {
							    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
					        data: {
					        	'firstName':$('input[name=firstName]').val(), 
					        	'lastName':$('input[name=lastName]').val(),
					        	'gender':$('input[name=gender]:checked').val(),
					        	'age':$('input[name=age]').val(),
					        	'email':$('input[name=email]').val(),
					        	'paidByOther':$('input[name=paidByOther]:checked').val(),
					        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
					        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
					        	'facebookEmail':$('input[name=facebookEmail]').val(),
					        	'sharePhone':$('input[name=sharePhone]:checked').val(),
					        	'phone':$('input[name=phone]').val(),
					        	'requestTeam':$('textarea[name=requestTeam]').val(),
					        	'extraHelp':$('textarea[name=extraHelp]').val(),
					        	'registrationDate':$('select[name=registrationDate]').val(),
					        	'referral':$('textarea[name=referral]').val(),
					        	'returningReferral':$('textarea[name=returningReferral]').val()
					        },
					        success: function(data){
					        	if(data === 'Success'){
					        		swal({
									  title: 'Thank You!',
									  text: 'You will be redirected to our website in 5 seconds',
									  timer: 5000,
									  onOpen: () => {
									    swal.showLoading()
									  }
									}).then((result) => {
									  if (result.dismiss === swal.DismissReason.timer) {
									    location.replace("https://hinttohabit.com");
									  }else{
									  	location.replace("https://hinttohabit.com");
									  }
									})
					        	}else{
					        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
					        	}
					        }
					    });
					}
				}else{
					if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
						swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
					}else{
						$.ajax({
					        url: '/submit-questionnaire',
					        type: "post",
					        headers: {
							    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
					        data: {
					        	'firstName':$('input[name=firstName]').val(), 
					        	'lastName':$('input[name=lastName]').val(),
					        	'gender':$('input[name=gender]:checked').val(),
					        	'age':$('input[name=age]').val(),
					        	'email':$('input[name=email]').val(),
					        	'paidByOther':$('input[name=paidByOther]:checked').val(),
					        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
					        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
					        	'facebookEmail':$('input[name=facebookEmail]').val(),
					        	'sharePhone':$('input[name=sharePhone]:checked').val(),
					        	'phone':$('input[name=phone]').val(),
					        	'requestTeam':$('textarea[name=requestTeam]').val(),
					        	'extraHelp':$('textarea[name=extraHelp]').val(),
					        	'registrationDate':$('select[name=registrationDate]').val(),
					        	'referral':$('textarea[name=referral]').val(),
					        	'returningReferral':$('textarea[name=returningReferral]').val()
					        },
					        success: function(data){
					        	if(data === 'Success'){
					        		swal({
									  title: 'Thank You!',
									  text: 'You will be redirected to our website in 5 seconds',
									  timer: 5000,
									  onOpen: () => {
									    swal.showLoading()
									  }
									}).then((result) => {
									  if (result.dismiss === swal.DismissReason.timer) {
									    location.replace("https://hinttohabit.com");
									  }else{
									  	location.replace("https://hinttohabit.com");
									  }
									})
					        	}else{
					        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
					        	}
					        }
					    });
					}
				}
			}else if($('input[name=facebookGroup]:checked').val() == null || $('input[name=facebookGroup]:checked').val() == ''){
				swal('Warning', 'Please say if you would like to join the Facebook group', 'warning');
			}else if($('input[name=sharePhone]:checked').val() == null || $('input[name=sharePhone]:checked').val() == ''){
				swal('Warning', 'Please say if you would like to share your phone number', 'warning');
			}else if($('input[name=sharePhone]:checked').val() == 1){
				if($('input[name=phone]').val() == null || $('input[name=phone]').val() == ''){
					swal('Warning', 'Please enter your phone number', 'warning');
				}else if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
					swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
				}else{
					$.ajax({
				        url: '/submit-questionnaire',
				        type: "post",
				        headers: {
						    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
				        data: {
				        	'firstName':$('input[name=firstName]').val(), 
				        	'lastName':$('input[name=lastName]').val(),
				        	'gender':$('input[name=gender]:checked').val(),
				        	'age':$('input[name=age]').val(),
				        	'email':$('input[name=email]').val(),
				        	'paidByOther':$('input[name=paidByOther]:checked').val(),
				        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
				        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
				        	'facebookEmail':$('input[name=facebookEmail]').val(),
				        	'sharePhone':$('input[name=sharePhone]:checked').val(),
				        	'phone':$('input[name=phone]').val(),
				        	'requestTeam':$('textarea[name=requestTeam]').val(),
				        	'extraHelp':$('textarea[name=extraHelp]').val(),
					        'registrationDate':$('select[name=registrationDate]').val(),
					        'referral':$('textarea[name=referral]').val(),
					        'returningReferral':$('textarea[name=returningReferral]').val()
				        },
				        success: function(data){
				        	if(data === 'Success'){
				        		swal({
								  title: 'Thank You!',
								  text: 'You will be redirected to our website in 5 seconds',
								  timer: 5000,
								  onOpen: () => {
								    swal.showLoading()
								  }
								}).then((result) => {
								  if (result.dismiss === swal.DismissReason.timer) {
								    location.replace("https://hinttohabit.com");
								  }else{
								  	location.replace("https://hinttohabit.com");
								  }
								})
				        	}else{
				        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
				        	}
				        }
				    });
				}
			}else if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
				swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
			}else{
				$.ajax({
			        url: '/submit-questionnaire',
			        type: "post",
			        headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
			        data: {
			        	'firstName':$('input[name=firstName]').val(), 
			        	'lastName':$('input[name=lastName]').val(),
			        	'gender':$('input[name=gender]:checked').val(),
			        	'age':$('input[name=age]').val(),
			        	'email':$('input[name=email]').val(),
			        	'paidByOther':$('input[name=paidByOther]:checked').val(),
			        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
			        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
			        	'facebookEmail':$('input[name=facebookEmail]').val(),
			        	'sharePhone':$('input[name=sharePhone]:checked').val(),
			        	'phone':$('input[name=phone]').val(),
			        	'requestTeam':$('textarea[name=requestTeam]').val(),
			        	'extraHelp':$('textarea[name=extraHelp]').val(),
					    'registrationDate':$('select[name=registrationDate]').val(),
					    'referral':$('textarea[name=referral]').val(),
					    'returningReferral':$('textarea[name=returningReferral]').val()
			        },
			        success: function(data){
			        	if(data === 'Success'){
			        		swal({
							  title: 'Thank You!',
							  text: 'You will be redirected to our website in 5 seconds',
							  timer: 5000,
							  onOpen: () => {
							    swal.showLoading()
							  }
							}).then((result) => {
							  if (result.dismiss === swal.DismissReason.timer) {
							    location.replace("https://hinttohabit.com");
							  }else{
							  	location.replace("https://hinttohabit.com");
							  }
							})
			        	}else{
			        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
			        	}
			        }
			    });
			}
		}
	}else if($('input[name=firstName]').val() == null || $('input[name=firstName]').val() == ''){
		swal('Warning', 'Please enter your first name', 'warning');
	}else if($('input[name=lastName]').val() == null || $('input[name=lastName]').val() == ''){
		swal('Warning', 'Please enter your last name', 'warning');
	}else if($('input[name=gender]:checked').val() == null || $('input[name=gender]:checked').val() == ''){
		swal('Warning', 'Please enter your gender', 'warning');
	}else if($('input[name=age]').val() == null || $('input[name=age]').val() == ''){
		swal('Warning', 'Please enter your age', 'warning');
	}else if($('input[name=email]').val() == null || $('input[name=email]').val() == ''){
		swal('Warning', 'Please enter your email', 'warning');
	}else if($('input[name=paidByOther]:checked').val() == null || $('input[name=paidByOther]:checked').val() == ''){
		swal('Warning', 'Please specify if someone paid for you', 'warning');
	}else if($('input[name=paidByOther]:checked').val() == 1){
		if($('input[name=paidByOtherName]').val() == null || $('input[name=paidByOtherName]').val() == ''){
			swal('Warning', 'Please enter the name of the person that paid for you', 'warning');
		}else if($('input[name=facebookGroup]:checked').val() == null || $('input[name=facebookGroup]:checked').val() == ''){
			swal('Warning', 'Please say if you would like to join the Facebook group', 'warning');
		}else if($('input[name=sharePhone]:checked').val() == null || $('input[name=sharePhone]:checked').val() == ''){
			swal('Warning', 'Please say if you would like to share your phone number', 'warning');
		}else if($('input[name=sharePhone]:checked').val() == 1){
			if($('input[name=phone]').val() == null || $('input[name=phone]').val() == ''){
				swal('Warning', 'Please enter your phone number', 'warning');
			}else if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
				swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
			}else{
				$.ajax({
			        url: '/submit-questionnaire',
			        type: "post",
			        headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
			        data: {
			        	'firstName':$('input[name=firstName]').val(), 
			        	'lastName':$('input[name=lastName]').val(),
			        	'gender':$('input[name=gender]:checked').val(),
			        	'age':$('input[name=age]').val(),
			        	'email':$('input[name=email]').val(),
			        	'paidByOther':$('input[name=paidByOther]:checked').val(),
			        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
			        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
			        	'facebookEmail':$('input[name=facebookEmail]').val(),
			        	'sharePhone':$('input[name=sharePhone]:checked').val(),
			        	'phone':$('input[name=phone]').val(),
			        	'requestTeam':$('textarea[name=requestTeam]').val(),
			        	'extraHelp':$('textarea[name=extraHelp]').val(),
			        	'registrationDate':$('select[name=registrationDate]').val(),
			        	'referral':$('textarea[name=referral]').val(),
					    'returningReferral':$('textarea[name=returningReferral]').val()
			        },
			        success: function(data){
			        	if(data === 'Success'){
			        		swal({
							  title: 'Thank You!',
							  text: 'You will be redirected to our website in 5 seconds',
							  timer: 5000,
							  onOpen: () => {
							    swal.showLoading()
							  }
							}).then((result) => {
							  if (result.dismiss === swal.DismissReason.timer) {
							    location.replace("https://hinttohabit.com");
							  }else{
							  	location.replace("https://hinttohabit.com");
							  }
							})
			        	}else{
			        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
			        	}
			        }
			    });
			}
		}else{
			if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
				swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
			}else{
				$.ajax({
			        url: '/submit-questionnaire',
			        type: "post",
			        headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
			        data: {
			        	'firstName':$('input[name=firstName]').val(), 
			        	'lastName':$('input[name=lastName]').val(),
			        	'gender':$('input[name=gender]:checked').val(),
			        	'age':$('input[name=age]').val(),
			        	'email':$('input[name=email]').val(),
			        	'paidByOther':$('input[name=paidByOther]:checked').val(),
			        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
			        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
			        	'facebookEmail':$('input[name=facebookEmail]').val(),
			        	'sharePhone':$('input[name=sharePhone]:checked').val(),
			        	'phone':$('input[name=phone]').val(),
			        	'requestTeam':$('textarea[name=requestTeam]').val(),
			        	'extraHelp':$('textarea[name=extraHelp]').val(),
			        	'registrationDate':$('select[name=registrationDate]').val(),
			        	'referral':$('textarea[name=referral]').val(),
					    'returningReferral':$('textarea[name=returningReferral]').val()
			        },
			        success: function(data){
			        	if(data === 'Success'){
			        		swal({
							  title: 'Thank You!',
							  text: 'You will be redirected to our website in 5 seconds',
							  timer: 5000,
							  onOpen: () => {
							    swal.showLoading()
							  }
							}).then((result) => {
							  if (result.dismiss === swal.DismissReason.timer) {
							    location.replace("https://hinttohabit.com");
							  }else{
							  	location.replace("https://hinttohabit.com");
							  }
							})
			        	}else{
			        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
			        	}
			        }
			    });
			}
		}
	}else if($('input[name=facebookGroup]:checked').val() == null || $('input[name=facebookGroup]:checked').val() == ''){
		swal('Warning', 'Please say if you would like to join the Facebook group', 'warning');
	}else if($('input[name=sharePhone]:checked').val() == null || $('input[name=sharePhone]:checked').val() == ''){
		swal('Warning', 'Please say if you would like to share your phone number', 'warning');
	}else if($('input[name=sharePhone]:checked').val() == 1){
		if($('input[name=phone]').val() == null || $('input[name=phone]').val() == ''){
			swal('Warning', 'Please enter your phone number', 'warning');
		}else if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
			swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
		}else{
			$.ajax({
		        url: '/submit-questionnaire',
		        type: "post",
		        headers: {
				    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		        data: {
		        	'firstName':$('input[name=firstName]').val(), 
		        	'lastName':$('input[name=lastName]').val(),
		        	'gender':$('input[name=gender]:checked').val(),
		        	'age':$('input[name=age]').val(),
		        	'email':$('input[name=email]').val(),
		        	'paidByOther':$('input[name=paidByOther]:checked').val(),
		        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
		        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
		        	'facebookEmail':$('input[name=facebookEmail]').val(),
		        	'sharePhone':$('input[name=sharePhone]:checked').val(),
		        	'phone':$('input[name=phone]').val(),
		        	'requestTeam':$('textarea[name=requestTeam]').val(),
		        	'extraHelp':$('textarea[name=extraHelp]').val(),
			        'registrationDate':$('select[name=registrationDate]').val(),
			        'referral':$('textarea[name=referral]').val(),
					'returningReferral':$('textarea[name=returningReferral]').val()
		        },
		        success: function(data){
		        	if(data === 'Success'){
		        		swal({
						  title: 'Thank You!',
						  text: 'You will be redirected to our website in 5 seconds',
						  timer: 5000,
						  onOpen: () => {
						    swal.showLoading()
						  }
						}).then((result) => {
						  if (result.dismiss === swal.DismissReason.timer) {
						    location.replace("https://hinttohabit.com");
						  }else{
						  	location.replace("https://hinttohabit.com");
						  }
						})
		        	}else{
		        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
		        	}
		        }
		    });
		}
	}else if($('textarea[name=extraHelp]').val() == null || $('textarea[name=extraHelp]').val() == ''){
		swal('Warning', 'Please enter some help or a goal that you have when it comes to your health and fitness goals', 'warning');
	}else{
		$.ajax({
	        url: '/submit-questionnaire',
	        type: "post",
	        headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
	        data: {
	        	'firstName':$('input[name=firstName]').val(), 
	        	'lastName':$('input[name=lastName]').val(),
	        	'gender':$('input[name=gender]:checked').val(),
	        	'age':$('input[name=age]').val(),
	        	'email':$('input[name=email]').val(),
	        	'paidByOther':$('input[name=paidByOther]:checked').val(),
	        	'paidByOtherName':$('input[name=paidByOtherName]').val(),
	        	'facebookGroup':$('input[name=facebookGroup]:checked').val(),
	        	'facebookEmail':$('input[name=facebookEmail]').val(),
	        	'sharePhone':$('input[name=sharePhone]:checked').val(),
	        	'phone':$('input[name=phone]').val(),
	        	'requestTeam':$('textarea[name=requestTeam]').val(),
	        	'extraHelp':$('textarea[name=extraHelp]').val(),
			    'registrationDate':$('select[name=registrationDate]').val(),
			    'referral':$('textarea[name=referral]').val(),
				'returningReferral':$('textarea[name=returningReferral]').val()
	        },
	        success: function(data){
	        	if(data === 'Success'){
	        		swal({
					  title: 'Thank You!',
					  text: 'You will be redirected to our website in 5 seconds',
					  timer: 5000,
					  onOpen: () => {
					    swal.showLoading()
					  }
					}).then((result) => {
					  if (result.dismiss === swal.DismissReason.timer) {
					    location.replace("https://hinttohabit.com");
					  }else{
					  	location.replace("https://hinttohabit.com");
					  }
					})
	        	}else{
	        		swal('Warning', 'Something went wrong. Please try again.', 'danger');
	        	}
	        }
	    });
	}
}