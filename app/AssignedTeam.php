<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignedTeam extends Model
{
    public function participant()
    {
        return $this->hasOne('App\Participant', 'id', 'participant_id')->with('results');
    }

    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }
}
