<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Participant;
use App\ArchivedParticipant;

class ArchiveParticipants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ArchiveParticipants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $participants = Participant::all();
        foreach($participants as $participant){
            $archived_participant = new ArchivedParticipant;
            $archived_participant->first_name = $participant->first_name;
            $archived_participant->last_name = $participant->last_name;
            $archived_participant->gender = $participant->gender;
            $archived_participant->age = $participant->age;
            $archived_participant->email = $participant->email;
            $archived_participant->paid_by_other = $participant->paid_by_other;
            $archived_participant->paid_by_other_name = $participant->paid_by_other_name;
            $archived_participant->facebook_group = $participant->facebook_group;
            $archived_participant->facebook_email = $participant->facebook_email;
            $archived_participant->share_phone = $participant->share_phone;
            $archived_participant->phone = $participant->phone;
            $archived_participant->team_request = $participant->team_request;
            $archived_participant->referred_by = $participant->referred_by;
            $archived_participant->referral = $participant->referral;
            $archived_participant->extra_help = $participant->extra_help;
            $archived_participant->disabled = $participant->disabled;
            $archived_participant->registered_dates = $participant->registered_dates;
            $archived_participant->save();
        }
        Participant::truncate();
    }
}
