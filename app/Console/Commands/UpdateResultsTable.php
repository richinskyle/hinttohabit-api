<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AssignedTeam;
use App\WeeklyResult;

class UpdateResultsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:UpdateResultsTable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $assigned_teams = AssignedTeam::all();
        $results = WeeklyResult::all();
        foreach($assigned_teams as $team_member){
            $result = WeeklyResult::where('participant_id', $team_member->participant_id)->first();
            if($team_member->team_id != $result->team_id){
                $result->team_id = $team_member->team_id;
                $result->save();
            }
            if($team_member->coach_id != $result->coach_id){
                $result->coach_id = $team_member->coach_id;
                $result->save();
            }
        }
    }
}
