<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AssignedTeam;

class Participant extends Model
{
    protected $appends = array('assigned_team');

    public function getAssignedTeamAttribute() {
        $team = AssignedTeam::with('team')->where('participant_id', $this->id)->first();
        return $team['team']['name'];
    }

    public function results()
    {
        return $this->hasOne('App\WeeklyResult', 'participant_id', 'id');
    }
}
