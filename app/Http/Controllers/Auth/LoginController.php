<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    public function authenticate(Request $request)
    {
        if (Auth::attempt(['username' => Request::get('username'), 'password' => Request::get('password')])) {
            // Authentication passed...
            $user = User::find(Auth::user()->id);
            return ['user' => $user, Config::get('session.token_name') => Helpers::generateWebToken($user->remember_token, Request::getClientIp())];
        }else{
            return 'fail';
            //return response('Invalid credentials', 401);
        }
    }
}
