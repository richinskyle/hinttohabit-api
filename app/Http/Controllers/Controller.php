<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Response;
use Exception;
use Illuminate\Http\Request;
use App\Participant;
use App\WaitingParticipant;
use App\Team;
use App\User;
use App\AssignedTeam;
use App\WeeklyResult;
use Redirect;
use Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function participants()
    {
        return Participant::orderBy('first_name', 'asc')->get();
    }

    public function participant($id)
    {
        return Participant::find($id);
    }

    public function teams()
    {
        return Team::with('members')->orderBy('id', 'asc')->get();
    }

    public function myTeams($id)
    {
        return Team::with('members')->where('coach_id', $id)->orderBy('id', 'asc')->get();
    }

    public function team($id)
    {
        return Team::with('members')->find($id);
    }

    public function assignedTeams()
    {
        return AssignedTeam::all();
    }

    public function leaderboard($type, $week)
    {
        $all_teams = $this->teams();
        if($all_teams[0]->groups == 1){
            $teams = $all_teams;
        }else{
            $teams = $request->input('teams'); 
        }

        foreach($teams as $team){
            foreach($team['members'] as $key => $member){
                if($member['participant']['disabled'] == 1){
                    unset($team['members'][$key]);
                }
            }
        }

        $participant_data = ['rows' => [], 'week' => $week];
        $team_array = [];
        foreach($teams as $team){
            $team_array[$team['name']] = new \stdClass;
            $team_array[$team['name']]->total_weight_change = NULL;
            $team_array[$team['name']]->total_points = 0;
            $team_array[$team['name']]->week_average_points = 0;
            $team_array[$team['name']]->week_weight_change = NULL;
            $team_array[$team['name']]->team_name = $team['name'];
            foreach($team['members'] as $member){
                $object = new \stdClass;
                $name = $member['participant']['first_name'] . ' ' . $member['participant']['last_name'];
                if($week == 1){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_1_weight']);
                    $total_weight_loss = $week_weight_loss;
                    $week_points = $member['participant']['results']['week_1_points'];
                    $total_points = $week_points;

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 2){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_1_weight'], $member['participant']['results']['week_2_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_2_weight']);
                    $week_points = $member['participant']['results']['week_2_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 2);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 3){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_2_weight'], $member['participant']['results']['week_3_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_3_weight']);
                    $week_points = $member['participant']['results']['week_3_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 3);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 4){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_3_weight'], $member['participant']['results']['week_4_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_4_weight']);
                    $week_points = $member['participant']['results']['week_4_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 4);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 5){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_4_weight'], $member['participant']['results']['week_5_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_5_weight']);
                    $week_points = $member['participant']['results']['week_5_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 5);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 6){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_5_weight'], $member['participant']['results']['week_6_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_6_weight']);
                    $week_points = $member['participant']['results']['week_6_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 6);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 7){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_6_weight'], $member['participant']['results']['week_7_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_7_weight']);
                    $week_points = $member['participant']['results']['week_7_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 7);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 8){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_7_weight'], $member['participant']['results']['week_8_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_8_weight']);
                    $week_points = $member['participant']['results']['week_8_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 8);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }
                $object->name = $name;
                $object->week_weight_loss = $week_weight_loss;
                $object->total_weight_loss = $total_weight_loss;
                $object->week_points = $week_points;
                $object->total_points = $total_points;
                array_push($participant_data['rows'], $object);
            }
            $team_array[$team['name']]->week_average_points = round($team_array[$team['name']]->week_average_points / count($team['members']), 2);
            $team_array[$team['name']]->coach_id = $team['coach_id'];

            $team_array[$team['name']]->participant_data = $participant_data;
            $participant_data = ['rows' => [], 'week' => $week];
        }
        return Response::json($team_array);
    }

    public function newTeam(Request $request)
    {
        // See if team exists
        $existing_team = Team::where('name', $request->input('teamName'))->first();
        if($existing_team == NULL){
            $team = new Team();
            $team->name = $request->input('teamName');
            if(is_numeric($team->name)){
                $team->coach_id = 1;
            }else{
                $team->coach_id = 2;
            }
            $team->save();

            return $this->teams();
        }else{
            return Response::json('Team exists');
        }
    }

    public function newTeamMember(Request $request)
    {
        $team_id = $request->input('teamId');
        $team = Team::find($team_id);
        $participant_id = $request->input('participantId');
        $coach_id = $team->coach_id;

        // If member is on a team already, change it to new team
        $assigned_team_member = AssignedTeam::where('participant_id', $participant_id)->first();
        if($assigned_team_member == NULL){
            $new_team_member = new AssignedTeam();
            $new_team_member->team_id = $team_id;
            $new_team_member->participant_id = $participant_id;
            $new_team_member->coach_id = $coach_id;
            $new_team_member->save();

            // Add weekly results row
            $results = new WeeklyResult();
            $results->participant_id = $participant_id;
            $results->team_id = $team_id;
            $results->coach_id = $coach_id;
            $results->save();
        }else{
            $assigned_team_member->team_id = $team_id;
            $assigned_team_member->coach_id = $coach_id;
            $assigned_team_member->save();

            $result_row = WeeklyResult::where('participant_id', $participant_id)->first();
            $result_row->coach_id = $coach_id;
            $result_row->team_id = $team_id;
            $result_row->save();
        }

        return $this->teams();
    }

    public function changeTeamCoach($id, Request $request)
    {
        $team_data = $request->input('teamData');
        // Update Team
        $team = Team::find($id);
        $team->coach_id = $team_data['coach_id'];
        $team->save();

        // Update Assigned Teams
        AssignedTeam::where('team_id', $team->id)->update(['coach_id' => $team_data['coach_id']]);

        // Update Result Rows
        WeeklyResult::where('team_id', $team->id)->update(['coach_id' => $team_data['coach_id']]);

        return $this->teams();
    }

    public function deleteTeam($id)
    {
        $team = Team::find($id);

        // Update assigned team table
        $assigned_team_rows = AssignedTeam::where('team_id', $team->id)->update(['team_id' => NULL, 'coach_id' => NULL]);
        // Update results table
        $weekly_results_rows = WeeklyResult::where('team_id', $team->id)->update(['team_id' => NULL, 'coach_id' => NULL]);

        // Delete the team
        $team->delete();

        return $this->teams();
    }

    public function randomizeTeams(Request $request)
    {
        $participants = Participant::orderBy('age', 'asc')->get();
        $number_of_groups = $request->input('numberOfGroups');
        // If 1 group, how many teams will be created?
        if($number_of_groups == 1){
            $created_teams = round(count($participants) / 10);
        }else{
            $created_teams = NULL;
        }

        // Delete pre-existing data
        Team::truncate();
        AssignedTeam::truncate();

        $existing_teams = Team::all();

        // Set the number teams
        $start_index = 0;
        $current_number_team = 1;
        $loop_count = 0;
        for ($i=0; $i < count($participants) / $number_of_groups; $i++) { 
            // see if current team exists
            if(Team::where('name', $current_number_team)->first()){
                // do nothing
            }else{
                $create_number_team = new Team();
                $create_number_team->name = $current_number_team;
                $create_number_team->groups = $number_of_groups;
                if($created_teams){
                    if($current_number_team <= ($created_teams/2)){
                        $create_number_team->coach_id = 1;
                    }else{
                        $create_number_team->coach_id = 2;
                    }
                }else{
                    $create_number_team->coach_id = 1;
                }
                $create_number_team->save();
            }
            $current_team = Team::where('name', $current_number_team)->first();

            $new_team = new AssignedTeam();
            $new_team->team_id = $current_team->id;
            $new_team->participant_id = $participants[$i]->id;
            if($created_teams){
                if($current_number_team <= ($created_teams/2)){
                    $new_team->coach_id = 1;
                }else{
                    $new_team->coach_id = 2;
                }
            }else{
                $new_team->coach_id = 1;
            }
            $new_team->save();

            // Add weekly results row (see if it exists first)
            if(WeeklyResult::where('participant_id', $participants[$i]->id)->first()){
                WeeklyResult::where('participant_id', $participants[$i]->id)->update(['team_id' => $new_team->team_id]);
            }else{
                $results = new WeeklyResult();
                $results->participant_id = $participants[$i]->id;
                $results->team_id = $current_team->id;
                if($created_teams){
                    if($current_number_team <= ($created_teams/2)){
                        $new_team->coach_id = 1;
                    }else{
                        $new_team->coach_id = 2;
                    }
                }else{
                    $new_team->coach_id = 1;
                }
                $results->save();
            }

            $start_index++;
            $loop_count++;
            if($current_number_team * 10 == $loop_count){
                $current_number_team++;
            }
        }

        if($number_of_groups > 1){
            // Set letter teams
            $current_letter_team = 'A';
            $current_letter_count = 1;
            $letter_loop_count = 0;
            if(is_numeric( count($participants) / $number_of_groups ) && floor( count($participants) / $number_of_groups ) != count($participants) / $number_of_groups){
                $stop_loop = round(count($participants) / $number_of_groups) - 1;
            }
            for ($x=$start_index - 1; $x < count($participants); $x++) {
                // see if current team exists
                if(Team::where('name', $current_letter_team)->first()){
                    // do nothing
                }else{
                    $create_letter_team = new Team();
                    $create_letter_team->name = $current_letter_team;
                    $create_letter_team->coach_id = 2;
                    $create_letter_team->groups = $number_of_groups;
                    $create_letter_team->save();
                }
                $current_team = Team::where('name', $current_letter_team)->first();

                $new_letter_team = new AssignedTeam();
                $new_letter_team->team_id = $current_team->id;
                $new_letter_team->participant_id = $participants[$x+1]->id;
                $new_letter_team->coach_id = 2;
                $new_letter_team->save();

                // Add weekly results row (see if it exists first)
                if(WeeklyResult::where('participant_id', $participants[$x+1]->id)->first()){
                    WeeklyResult::where('participant_id', $participants[$x+1]->id)->update(['team_id' => $new_team->team_id]);
                }else{
                    $results = new WeeklyResult();
                    $results->participant_id = $participants[$x+1]->id;
                    $results->team_id = $current_team->id;
                    $results->coach_id = 2;
                    $results->save();
                }

                $letter_loop_count++;
                if($current_letter_count * 10 == $letter_loop_count){
                    ++$current_letter_team;
                }
                if($letter_loop_count == $stop_loop){
                    return $this->teams();
                }
            }
        }

        return $this->teams();
    }

    public function updateTeamData($id, Request $request)
    {
        foreach($request->input('data.members') as $member){
            $results_row = WeeklyResult::where('participant_id', $member['participant']['id'])->first();
            $results_row->initial_weight = $member['participant']['results']['initial_weight'];
            $results_row->week_1_weight = $member['participant']['results']['week_1_weight'];
            $results_row->week_1_points = $member['participant']['results']['week_1_points'];
            $results_row->week_2_weight = $member['participant']['results']['week_2_weight'];
            $results_row->week_2_points = $member['participant']['results']['week_2_points'];
            $results_row->week_3_weight = $member['participant']['results']['week_3_weight'];
            $results_row->week_3_points = $member['participant']['results']['week_3_points'];
            $results_row->week_4_weight = $member['participant']['results']['week_4_weight'];
            $results_row->week_4_points = $member['participant']['results']['week_4_points'];
            $results_row->week_5_weight = $member['participant']['results']['week_5_weight'];
            $results_row->week_5_points = $member['participant']['results']['week_5_points'];
            $results_row->week_6_weight = $member['participant']['results']['week_6_weight'];
            $results_row->week_6_points = $member['participant']['results']['week_6_points'];
            $results_row->week_7_weight = $member['participant']['results']['week_7_weight'];
            $results_row->week_7_points = $member['participant']['results']['week_7_points'];
            $results_row->week_8_weight = $member['participant']['results']['week_8_weight'];
            $results_row->week_8_points = $member['participant']['results']['week_8_points'];
            $results_row->save();
        }
        return $this->team($id);
    }

    public function updateMyTeamsData(Request $request)
    {
        $my_teams_array = $request->input('data');
        foreach($my_teams_array as $team){
            foreach($team['members'] as $member){
                $results_row = WeeklyResult::where('participant_id', $member['participant']['id'])->first();
                $results_row->initial_weight = $member['participant']['results']['initial_weight'];
                $results_row->week_1_weight = $member['participant']['results']['week_1_weight'];
                $results_row->week_1_points = $member['participant']['results']['week_1_points'];
                $results_row->week_2_weight = $member['participant']['results']['week_2_weight'];
                $results_row->week_2_points = $member['participant']['results']['week_2_points'];
                $results_row->week_3_weight = $member['participant']['results']['week_3_weight'];
                $results_row->week_3_points = $member['participant']['results']['week_3_points'];
                $results_row->week_4_weight = $member['participant']['results']['week_4_weight'];
                $results_row->week_4_points = $member['participant']['results']['week_4_points'];
                $results_row->week_5_weight = $member['participant']['results']['week_5_weight'];
                $results_row->week_5_points = $member['participant']['results']['week_5_points'];
                $results_row->week_6_weight = $member['participant']['results']['week_6_weight'];
                $results_row->week_6_points = $member['participant']['results']['week_6_points'];
                $results_row->week_7_weight = $member['participant']['results']['week_7_weight'];
                $results_row->week_7_points = $member['participant']['results']['week_7_points'];
                $results_row->week_8_weight = $member['participant']['results']['week_8_weight'];
                $results_row->week_8_points = $member['participant']['results']['week_8_points'];
                $results_row->save();
            }
        }
        return $this->myTeams($my_teams_array[0]['coach_id']);
    }

    public function memberDropout($id, Request $request)
    {
        $assigned_team = AssignedTeam::where('participant_id', $id)->first();
        $assigned_team->delete();

        return $this->myTeams($request->input('coachId'));
    }

    public function disableParticipant($id, Request $request)
    {
        $coach_id = $request->input('coachId');
        $type = $request->input('type');
        $participant = Participant::find($id);
        if($type == 'disable'){
            $participant->disabled = 1;
        }elseif($type == 'enable'){
            $participant->disabled = 0;
        }
        $participant->save();

        return $this->myTeams($coach_id);
    }

    public function submitQuestionnaire(Request $request)
    {
    	$participant = new WaitingParticipant();
    	$participant->first_name = $request->input('firstName');
    	$participant->last_name = $request->input('lastName');
    	$participant->gender = $request->input('gender');
    	$participant->age = $request->input('age');
    	$participant->email = $request->input('email');
    	$participant->paid_by_other = $request->input('paidByOther');
    	$participant->paid_by_other_name = $request->input('paidByOtherName');
    	$participant->facebook_group = $request->input('facebookGroup');
    	$participant->facebook_email = $request->input('facebookEmail');
    	$participant->share_phone = $request->input('sharePhone');
    	$participant->phone = $request->input('phone');
    	$participant->team_request = $request->input('requestTeam');
        $participant->extra_help = $request->input('extraHelp');
        $participant->disabled = 0;
        $participant->registered_dates = $request->input('registrationDate');
        $participant->referred_by = $request->input('referral');
    	$participant->referral = $request->input('returningReferral');
    	$participant->save();

    	return Response::json('Success');
    }

    public function sendTeamEmail($id, Request $request)
    {
        $team = Team::find($id);
        $email_content = $request->input('emailContent');
        $email_body = $email_content['email'];
        $data = ['content' => $email_body];
        $subject = "A message from your coach";
        $number_of_emails = 0;
        $failed_emails = [];
        foreach($email_content['team']['members'] as $member){
            $email_address = $member['participant']['email'];
            try {
                Mail::send('emails.general-email', $data, function ($message) use ($email_address, $subject) {
                    $message->to($email_address)->subject($subject);
                });
                $number_of_emails++;
            } catch (Exception $e) {
                $admin_email = 'hinttohabit@gmail.com';
                $admin_subject = 'Failed To Send Team Email To: ' . $email_address;
                Mail::send('emails.general-email', $data, function ($message) use ($admin_email, $admin_subject) {
                    $message->to($admin_email)->subject($admin_subject);
                });
                array_push($failed_emails, $email_address);
            }
        }
        return Response::json(array('successfulEmails' => $number_of_emails, 'failedEmails' => $failed_emails));
    }

    public function emailResults(Request $request)
    {
        $all_teams = $this->teams();
        if($all_teams[0]->groups == 1){
            $teams = $all_teams;
        }else{
            $teams = $request->input('teams'); 
        }

        foreach($teams as $team){
            foreach($team['members'] as $key => $member){
                if($member['participant']['disabled'] == 1){
                    unset($team['members'][$key]);
                }
            }
        }

        $week = $request->input('week');
        $custom_message = $request->input('message');
        $coach_id = $request->input('coachId');
        $subject = 'Weekly Results';
        $email_data = ['rows' => [], 'week' => $week, 'teams' => $teams];
        $email_addresses = [];
        $team_array = [];
        foreach($teams as $team){
            $team_array[$team['name']] = new \stdClass;
            $team_array[$team['name']]->total_weight_change = NULL;
            $team_array[$team['name']]->total_points = 0;
            $team_array[$team['name']]->week_average_points = 0;
            $team_array[$team['name']]->week_weight_change = NULL;
            foreach($team['members'] as $member){
                if($member['participant']['results']['initial_weight'] == NULL || $member['participant']['results']['initial_weight'] == 0){
                    return Response::json($member['participant']['first_name'] . ' ' . $member['participant']['last_name'] . ' has no initial weight');
                }
                $object = new \stdClass;
                $first_name = $member['participant']['first_name'] . ' ' . $member['participant']['last_name'];
                $email_address = $member['participant']['email'];
                if($week == 1){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_1_weight']);
                    $total_weight_loss = $week_weight_loss;
                    $week_points = $member['participant']['results']['week_1_points'];
                    $total_points = $week_points;

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 2){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_1_weight'], $member['participant']['results']['week_2_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_2_weight']);
                    $week_points = $member['participant']['results']['week_2_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 2);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 3){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_2_weight'], $member['participant']['results']['week_3_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_3_weight']);
                    $week_points = $member['participant']['results']['week_3_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 3);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 4){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_3_weight'], $member['participant']['results']['week_4_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_4_weight']);
                    $week_points = $member['participant']['results']['week_4_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 4);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 5){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_4_weight'], $member['participant']['results']['week_5_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_5_weight']);
                    $week_points = $member['participant']['results']['week_5_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 5);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 6){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_5_weight'], $member['participant']['results']['week_6_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_6_weight']);
                    $week_points = $member['participant']['results']['week_6_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 6);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 7){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_6_weight'], $member['participant']['results']['week_7_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_7_weight']);
                    $week_points = $member['participant']['results']['week_7_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 7);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }elseif($week == 8){
                    $week_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['week_7_weight'], $member['participant']['results']['week_8_weight']);
                    $total_weight_loss = $this->calcWeightLossPercentage($member['participant']['results']['initial_weight'], $member['participant']['results']['week_8_weight']);
                    $week_points = $member['participant']['results']['week_8_points'];
                    $total_points = $this->calcPointsEarned($member['participant']['results'], 8);

                    $team_array[$team['name']]->week_weight_change += $week_weight_loss;
                    $team_array[$team['name']]->week_average_points += $week_points;

                    $team_array[$team['name']]->total_weight_change += $total_weight_loss;
                    $team_array[$team['name']]->total_points += $total_points;
                    $team_array[$team['name']]->total_average_points = round($team_array[$team['name']]->total_points / count($team['members']), 1);
                }
                $object->first_name = $first_name;
                $object->email_address = $email_address;
                $object->week_weight_loss = $week_weight_loss;
                $object->total_weight_loss = $total_weight_loss;
                $object->week_points = $week_points;
                $object->total_points = $total_points;
                array_push($email_data['rows'], $object);
                array_push($email_addresses, $email_address);
            }
            $team_array[$team['name']]->week_average_points = round($team_array[$team['name']]->week_average_points / count($team['members']), 2);
            $team_array[$team['name']]->coach_id = $team['coach_id'];

            $team_array[$team['name']]->email_addresses = $email_addresses;
            $team_array[$team['name']]->email_data = $email_data;
            $email_addresses = [];
            $email_data = ['rows' => [], 'week' => $week, 'teams' => $teams];
        }

        foreach($team_array as $key => $team){
            if($all_teams[0]->groups == 1){
                if($team->coach_id == $coach_id){
                    $data = ['team_objects' => $team, 'team_array' => $team_array, 'custom_message' => $custom_message];
                    foreach($team->email_addresses as $email){
                        try {
                            Mail::send('emails.weekly-results', $data, function ($message) use ($email, $subject) {
                                $message->to($email)->subject($subject);
                            });
                        } catch (Exception $e) {
                            $admin_email = 'hinttohabit@gmail.com';
                            $admin_subject = 'Failed To Send Results Email To: ' . $email;
                            Mail::send('emails.weekly-results', $data, function ($message) use ($admin_email, $admin_subject) {
                                $message->to($admin_email)->subject($admin_subject);
                            });
                        }
                    }
                }
            }else{
                $data = ['team_objects' => $team, 'team_array' => $team_array, 'custom_message' => $custom_message];
                foreach($team->email_addresses as $email){
                    try {
                        Mail::send('emails.weekly-results', $data, function ($message) use ($email, $subject) {
                            $message->to($email)->subject($subject);
                        });
                    } catch (Exception $e) {
                        $admin_email = 'hinttohabit@gmail.com';
                        $admin_subject = 'Failed To Send Results Email To: ' . $email;
                        Mail::send('emails.weekly-results', $data, function ($message) use ($admin_email, $admin_subject) {
                            $message->to($admin_email)->subject($admin_subject);
                        });
                    }
                }
            }
        }

        return Response::json('Success');
    }

    public function introEmail(Request $request)
    {
        $teams = $request->input('teams');
        $email_content = $request->input('email');
        $email_addresses = [];
        $subject = 'Team Introduction';
        $email_data = ['rows' => [], 'team' => NULL, 'coach' => NULL, 'content' => $email_content];
        $successful_emails = 0;
        foreach($teams as $team){
            $email_data['team'] = $team['name'];
            $coach = User::find($team['coach_id']);
            $email_data['coach'] = $coach->first_name;
            foreach($team['members'] as $member){
                $object = new \stdClass;
                $email_address = $member['participant']['email'];
                $first_name = $member['participant']['first_name'];
                $last_name = $member['participant']['last_name'];
                $phone = preg_replace('/[^\dxX]/', '', $member['participant']['phone']);

                $object->first_name = $first_name;
                $object->last_name = $last_name;
                $object->email_address = $email_address;
                $object->share_phone = $member['participant']['share_phone'];;
                $object->phone = $phone;

                array_push($email_data['rows'], $object);
                array_push($email_addresses, $email_address);
            }
            foreach($email_addresses as $email){
                try {
                    Mail::send('emails.intro-email', $email_data, function ($message) use ($email, $subject) {
                        $message->to($email)->subject($subject);
                    });
                    $successful_emails++;
                } catch (Exception $e) {
                    $admin_email = 'hinttohabit@gmail.com';
                    $admin_subject = 'Failed To Send Intro Email To: ' . $email;
                    Mail::send('emails.general-email', $email_data, function ($message) use ($admin_email, $admin_subject) {
                        $message->to($admin_email)->subject($admin_subject);
                    });
                }
            }
            $email_addresses = [];
            $email_data = ['rows' => [], 'team' => NULL, 'coach' => NULL, 'content' => $email_content];
        }

        return Response::json('Successfully sent ' . $successful_emails . ' emails');
    }

    public function reminderEmail(Request $request)
    {
        $teams = $request->input('teams');
        $email_content = $request->input('emailContent');
        $email_addresses = [];
        $subject = 'Reminder';
        $email_data = ['content' => $email_content];
        $successful_emails = 0;
        foreach($teams as $team){
            foreach($team['members'] as $member){
                $email_address = $member['participant']['email'];
                array_push($email_addresses, $email_address);
            }
            foreach($email_addresses as $email){
                try {
                    Mail::send('emails.general-email', $email_data, function ($message) use ($email, $subject) {
                        $message->to($email)->subject($subject);
                    });
                    $successful_emails++;
                } catch (Exception $e) {
                    $admin_email = 'hinttohabit@gmail.com';
                    $admin_subject = 'Failed To Send Reminder Email To: ' . $email;
                    Mail::send('emails.general-email', $email_data, function ($message) use ($admin_email, $admin_subject) {
                        $message->to($admin_email)->subject($admin_subject);
                    });
                }
            }
            $email_addresses = [];
        }

        return Response::json('Successfully sent ' . $successful_emails . ' emails');
    }

    public function refreshList(Request $request)
    {
        $waiting_participants = WaitingParticipant::all();
        foreach($waiting_participants as $row){
            $new_participant = new Participant();
            $new_participant->first_name = $row->first_name;
            $new_participant->last_name = $row->last_name;
            $new_participant->gender = $row->gender;
            $new_participant->age = $row->age;
            $new_participant->email = $row->email;
            $new_participant->paid_by_other = $row->paid_by_other;
            $new_participant->paid_by_other_name = $row->paid_by_other_name;
            $new_participant->facebook_group = $row->facebook_group;
            $new_participant->facebook_email = $row->facebook_email;
            $new_participant->share_phone = $row->share_phone;
            $new_participant->phone = $row->phone;
            $new_participant->team_request = $row->team_request;
            $new_participant->referred_by = $row->referred_by;
            $new_participant->referral = $row->referral;
            $new_participant->extra_help = $row->extra_help;
            $new_participant->disabled = $row->disabled;
            $new_participant->registered_dates = $row->registered_dates;
            $new_participant->save();
        }
        WaitingParticipant::truncate();
        return $this->participants();
    }

    private function calcWeightLossPercentage($init_weight, $curr_weight)
    {
        return round((((1.0 - ($curr_weight / $init_weight)) * 100) * -1), 2);
    }

    private function calcPointsEarned($results, $week)
    {
        if($week == 1){
            return $results['week_1_points'];
        }elseif($week == 2){
            return $results['week_1_points'] + $results['week_2_points'];
        }elseif($week == 3){
            return $results['week_1_points'] + $results['week_2_points'] + $results['week_3_points'];
        }elseif($week == 4){
            return $results['week_1_points'] + $results['week_2_points'] + $results['week_3_points'] + $results['week_4_points'];
        }elseif($week == 5){
            return $results['week_1_points'] + $results['week_2_points'] + $results['week_3_points'] + $results['week_4_points'] + $results['week_5_points'];
        }elseif($week == 6){
            return $results['week_1_points'] + $results['week_2_points'] + $results['week_3_points'] + $results['week_4_points'] + $results['week_5_points'] + $results['week_6_points'];
        }elseif($week == 7){
            return $results['week_1_points'] + $results['week_2_points'] + $results['week_3_points'] + $results['week_4_points'] + $results['week_5_points'] + $results['week_6_points'] + $results['week_7_points'];
        }elseif($week == 8){
            return $results['week_1_points'] + $results['week_2_points'] + $results['week_3_points'] + $results['week_4_points'] + $results['week_5_points'] + $results['week_6_points'] + $results['week_7_points'] + $results['week_8_points'];;
        }
    }
}
