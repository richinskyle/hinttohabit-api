<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Response;
use Request;
use Auth;
use App\User;
use Config;
use App\Helpers;

class UserController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function authenticate(Request $request)
    {
        if (Auth::attempt(['username' => Request::get('username'), 'password' => Request::get('password')])) {
            // Authentication passed...
            $user = User::find(Auth::user()->id);
            $user->remember_token = str_random(100);
            $user->save();
            return ['user' => $user, Config::get('session.token_name') => Helpers::generateWebToken($user->remember_token, Request::getClientIp())];
        }else{
            return Response::json('fail');
            //return response('Invalid credentials', 401);
        }
    }

    public function currentUser()
    {
        $remember_token = Helpers::extractRememberToken(Request::header('Authorization'));
        return Response::json(User::where('remember_token', '=', $remember_token)->first());
    }
}
