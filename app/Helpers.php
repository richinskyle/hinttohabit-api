<?php

namespace App;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Exception;
use DateTime;
use App\Models\User;

class Helpers
{
    /**
     * Generate web token
     *
     * @param $remember_token
     * @param $client_ip
     * @return array
     */
    public static function generateWebToken($remember_token, $client_ip)
    {
        $date = new DateTime;
        $date->modify('+12 hours'); // Token expiration date: adjust as needed
        $formatted_date = $date->format('Y-m-d H:i:s');
        return Crypt::encrypt($remember_token . "|" . $client_ip . "|" . Config::get('app.secondary_key') . "|" . $formatted_date);
    }

    /**
     * Validate encrypted web token
     *
     * @param $token
     * @param Request $request
     * @return array
     */
    public static function validateWebToken($token, Request $request)
    {
        if (isset($token)) {
            try {
                $decrypted_token = Crypt::decrypt($token);
            } catch (Exception $e) {
                return false;
            }

            $decrypted_token = explode("|", $decrypted_token);
            $remember_token = $decrypted_token[0];
            $token_ip = $decrypted_token[1];
            $app_key = $decrypted_token[2];
            $token_date = new DateTime($decrypted_token[3]);
            $date = new DateTime;
            $client_ip = $request->getClientIp();

            if ($client_ip === $token_ip and $app_key === Config::get('app.secondary_key')) {
                $users = User::whereRememberToken($remember_token)->get();

                if ($users->count() === 1) {
                    $user = $users[0];

                    if ($date < $token_date) {
                        Auth::onceUsingId($user->id);
                        return true;
                    } else {
                        $user->remember_token = str_random(100);
                        $user->save();
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get remember_token from encrypted web token
     *
     * @param $token
     * @return array
     */
    public static function extractRememberToken($token)
    {
        $decrypted_token = explode("|", Crypt::decrypt($token));
        return $decrypted_token[0];
    }
}