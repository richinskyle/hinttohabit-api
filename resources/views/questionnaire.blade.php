<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Hint To Habit - Questionnaire</title>

        <!-- Fonts -->
        <link rel="shortcut icon" href="https://hinttohabit.com/wp-content/uploads/2018/02/Favicon.png" type="image/x-icon" />
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1040750879409032'); 
        fbq('track', 'PageView');
        </script>
        <noscript>
         <img height="1" width="1" 
        src="https://www.facebook.com/tr?id=1040750879409032&ev=PageView
        &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body style="background-color:#aaa9a9;">
        <script>
        fbq('track', 'Purchase');
        </script>
        <div class="container" style="margin-top:5%;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <img style="margin-left:auto; margin-right:auto; display:block; width:25%;" src="https://hinttohabit.com/wp-content/uploads/2018/02/Header-Logo-x2.png">
                </div>
                <div class="panel-body" style="background-color: white;">
                    <p>
                        We are so excited to have you join us for 8 weeks as we work together to turn healthy habits into an everyday lifestyle! Please fill out the questionnaire below ASAP (even if you have participated in the past, please fill it out again). We will be sending out an email before the challenge begins with any additional info you are going to need, including your team assignment. In the meantime, please feel free to email us at hinttohabit@gmail.com if you have any questions!
                    </p>
                    <p>
                        ALSO - PLEASE ONLY SUBMIT THIS FORM ONE TIME. Thanks so much!
                    </p>
                    <p>* indicates required field</p>
                    <form name="questionnaireForm">
                        @csrf
                        <div class="col-sm-12 col-md-12">
                            <div style="margin-bottom: 15px;">
                                <label>*Which challenge are you registering for? </label>
                                <select class="form-control" name="registrationDate">
                                    @foreach($dates as $date)
                                    <option value="{{$date->id}}">{{ $date->start_date }} - {{ $date->end_date }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*Are you a new participant or a returning participant? </label>
                                <span style="margin-left:10px;">
                                    <label class="radio-inline"><input id="newParticipant" type="radio" name="newParticipant" value="New">New Participant</label>
                                    <label class="radio-inline"><input id="returningParticipant" type="radio" name="newParticipant" value="Returning">Returning Participant</label>
                                </span>
                            </div>
                            <div id="newParticipantDetails" style="margin-bottom: 15px;">
                                <label>*Please tell us how you heard about our challenge. If you were referred by someone, please list their first and last name: </label>
                                <textarea class="form-control" name="referral"></textarea>
                            </div>
                            <div id="returningParticipantDetails" style="margin-bottom: 15px;">
                                <label>If you referred someone to do the challenge, please list their name(s) here: </label>
                                <textarea class="form-control" name="returningReferral"></textarea>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*First Name: </label>
                                <input class="form-control" placeholder="First Name" name="firstName" />
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*Last Name: </label>
                                <input class="form-control" placeholder="Last Name" name="lastName" />
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*Gender: </label>
                                <span style="margin-left:10px;">
                                    <label class="radio-inline"><input type="radio" name="gender" value="Male">Male</label>
                                    <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
                                    <label class="radio-inline"><input type="radio" name="gender" value="Undecided">Prefer not to specify</label>
                                </span>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*Age: </label>
                                <input class="form-control" placeholder="Age" name="age" />
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*Best email address to reach you at (please make sure you double check it is entered correctly, as this is how we will communicate with you): </label>
                                <input class="form-control" placeholder="Email" name="email" />
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*Is there someone else who paid for you to do this program?: </label>
                                <span style="margin-left:10px;">
                                    <label class="radio-inline"><input type="radio" name="paidByOther" value="1">Yes</label>
                                    <label class="radio-inline"><input type="radio" name="paidByOther" value="0">No</label>
                                </span>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>If yes, who was it? (Please leave their name so we can match it to our records and make sure we get your info correctly added to the group): </label>
                                <input class="form-control" placeholder="Payer's Name" name="paidByOtherName" />
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*Would you like to be part of our private Facebook group?: </label>
                                <span style="margin-left:10px;">
                                    <label class="radio-inline"><input type="radio" name="facebookGroup" value="1">Yes</label>
                                    <label class="radio-inline"><input type="radio" name="facebookGroup" value="0">No</label>
                                </span>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>If yes, what is the email address associated with your Facebook page (if different than above)?: </label>
                                <input class="form-control" placeholder="Facebook Email" name="facebookEmail" />
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>*As one of the ways to earn a point/checkmark each day, you have to be in contact with your teammates. Are you comfortable with your teammates having your cell phone number in case they want to send you a text message to encourage you and motivate you?: </label>
                                <span style="margin-left:10px;">
                                    <label class="radio-inline"><input type="radio" name="sharePhone" value="1">Yes</label>
                                    <label class="radio-inline"><input type="radio" name="sharePhone" value="0">No</label>
                                </span>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>If yes, what is the best number to reach you?: </label>
                                <input class="form-control" placeholder="Phone Number" name="phone" />
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>Is there anyone else who has signed up that you would like to have on your team? If yes, please leave their names below (be sure to use their full name and/or nickname so we can put you on the same team): </label>
                                <textarea class="form-control" name="requestTeam"></textarea>
                            </div>
                            <!-- <div style="margin-bottom: 15px;">
                                <label>If this is your first challenge, did someone refer you to this challenge? If yes, please enter their full name here: </label>
                                <input class="form-control" placeholder="Referred By..." name="referral" />
                            </div> -->
                            <div style="margin-bottom: 15px;">
                                <label>*What is something you need extra help with when it comes to your health and fitness goals?: </label>
                                <textarea class="form-control" name="extraHelp"></textarea>
                            </div>
                        </div>
                    </form>
                    <button class="btn btn-primary" style="float:right;" onclick="submit()">Submit</button>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="{{ asset('js/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
    </body>
</html>
