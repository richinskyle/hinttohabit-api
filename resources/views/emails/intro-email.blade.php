<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <style>
        	table {
			    border-collapse: collapse;
			    width: 100%;
			    display: table !important;
			}

			table, th, td {
			    border: 1px solid black;
			}
			th {
				height: 50px;
			}
			th, td {
				padding: 15px;
				text-align: left;
			}
        </style>
    </head>
    <body class="container">
    	<div class="panel panel-default">
			<div class="panel-heading">
				<img style="margin-left:auto; margin-right:auto; display:block;" src="http://striveforstrength.pandodev.com/wp-content/uploads/2018/02/Header-Logo.png">
			</div>
			<div class="panel-body">
				{!! $content !!}
				<br/><br/>
				<p><strong>Your Coach: {{ $coach }}</strong></p>
				<p><strong>Your Team: {{ $team }}</strong></p>
				<p><strong>Here are your teammates:</strong></p>
				<br/>
				<div class="table-responsive">
					<table class="table table-bordered" style="margin-left:auto; margin-right:auto; display:block;">
						<thead>
							<th>Team Member</th>
							<th>Email Address</th>
							<th>Phone Number</th>
						</thead>
						<tbody>
							@foreach($rows as $member)
							<tr>
								<td>{{ $member->first_name }} {{ $member->last_name }}</td>
								<td>{{ $member->email_address }}</td>
								<td>
									@if($member->share_phone == 1)
									{{ substr($member->phone, 0, 3) }}-{{ substr($member->phone, 3, 3) }}-{{ substr($member->phone, 6, 10) }}
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </body>
</html>