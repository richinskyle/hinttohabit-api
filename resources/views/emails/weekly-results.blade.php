<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <style>
        	table {
			    border-collapse: collapse;
			    width: 100%;
			    display: table !important;
			}

			table, th, td {
			    border: 1px solid black;
			}
			th {
				height: 50px;
			}
			th, td {
				padding: 15px;
				text-align: left;
			}
        </style>
    </head>
    <body class="container">
    	<div class="panel panel-default">
			<div class="panel-heading">
				<img style="margin-left:auto; margin-right:auto; display:block;" src="http://striveforstrength.pandodev.com/wp-content/uploads/2018/02/Header-Logo.png">
			</div>
			<br/><br/>
			<div class="panel-body">
				<p>Hey there! Here is a snapshot of the results from this past week of the challenge!</p>
				<br/><br/>
				<div class="table-responsive">
					<table class="table table-bordered" style="margin-left:auto; margin-right:auto; display:block;">
						<thead>
							<th>Team Member</th>
							<th>This Week's Weight Change %</th>
							<th>This Week's Earned Points</th>
							<th>Total % Weight Change</th>
							<th>Total Points Earned</th>
						</thead>
						<tbody>
							@foreach($team_objects->email_data['rows'] as $member)
							<tr>
								<td>{{ $member->first_name }}</td>
								<td>{{ $member->week_weight_loss }}%</td>
								<td>{{ $member->week_points }}</td>
								<td>{{ $member->total_weight_loss }}%</td>
								<td>{{ $member->total_points }}</td>
							</tr>
							@endforeach
							<tr>
								<td><strong>Totals:</strong></td>
								<td><strong>Avg:</strong> {{ round($team_objects->week_weight_change / count($team_objects->email_data['rows']), 2) }}%</td>
								<td><strong>Avg: </strong>{{ $team_objects->week_average_points }}</td>
								<td><strong>Avg:</strong> {{ round($team_objects->total_weight_change / count($team_objects->email_data['rows']), 2)  }}%</td>
								<td><strong>Avg: </strong>{{ $team_objects->total_average_points }}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br/><br/>
				<div class="table-responsive">
					<table class="table table-bordered" style="margin-left:auto; margin-right:auto; display:block;">
						<caption><strong>Team Totals</strong></caption>
						<thead>
							<th>Team</th>
							<th>Total Average Weight Change %</th>
							<th>Total Average Points</th>
						</thead>
						<tbody>
							@foreach($team_array as $key => $value)
								<tr>
									<td>{{ $key }}</td>
									<td>{{ round($value->total_weight_change / count($value->email_data['rows']), 2) }}%</td>
									<td>{{ $value->total_average_points }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			@if($custom_message != NULL && $custom_message != '')
			<div>
				<p>Here is a message from your coach:</p>
				<p>{{ $custom_message }}</p>
			</div>
			@endif
		</div>
    </body>
</html>