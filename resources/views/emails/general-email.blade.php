<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body class="container">
    	<div class="panel panel-default">
			<div class="panel-heading">
				<img style="margin-left:auto; margin-right:auto; display:block;" src="https://hinttohabit.com/wp-content/uploads/2018/02/Header-Logo.png">
			</div>
			<div class="panel-body" style="margin-top:25px;">
				{!! $content !!}
			</div>
		</div>
    </body>
</html>