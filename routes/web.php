<?php

use App\ChallengeDate;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/register', function () {
	$dates = ChallengeDate::where('archived', 0)->where('active', 0)->get();
    return view('questionnaire', ['dates' => $dates]);
});
Route::post('/submit-questionnaire', 'Controller@submitQuestionnaire');
Route::post('login', 'UserController@authenticate');
Route::get('current-user', 'UserController@currentUser');

// We'll lock this section down
Route::get('participants', 'Controller@participants');
Route::get('participant/{id}', 'Controller@participant');
Route::get('assigned-teams', 'Controller@assignedTeams');
Route::get('teams', 'Controller@teams');
Route::get('my-teams/{id}', 'Controller@myTeams');
Route::get('team/{id}', 'Controller@team');
Route::get('leaderboard/{type}/week/{week}', 'Controller@leaderboard');
Route::post('new-team', 'Controller@newTeam');
Route::post('new-team-member', 'Controller@newTeamMember');
Route::post('randomize-teams', 'Controller@randomizeTeams');
Route::post('update-team-data/{id}', 'Controller@updateTeamData');
Route::post('update-my-teams-data', 'Controller@updateMyTeamsData');
Route::post('member-dropout/{id}', 'Controller@memberDropout');
Route::post('email-results', 'Controller@emailResults');
Route::post('intro-email', 'Controller@introEmail');
Route::post('reminder-email', 'Controller@reminderEmail');
Route::post('send-team-email/{id}', 'Controller@sendTeamEmail');
Route::post('disable-participant/{id}', 'Controller@disableParticipant');
Route::post('change-team-coach/{id}', 'Controller@changeTeamCoach');
Route::post('delete-team/{id}', 'Controller@deleteTeam');
Route::post('refresh-list', 'Controller@refreshList');

//Auth::routes();
